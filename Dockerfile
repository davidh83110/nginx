FROM 990090895087.dkr.ecr.ap-southeast-1.amazonaws.com/david-nginx:latest

COPY nginx.conf /etc/nginx/nginx.conf


EXPOSE 80

CMD (tail -F /var/log/nginx/access.log &) && exec nginx -g "daemon off;"
